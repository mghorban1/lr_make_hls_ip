This package tests a version of the Linear-Regression (LR) HLS code with top-level implementation of interface:

    LRHLS_v0(const SettingsHLS *settingsHLS, DataHLS *dataHLS);

And creates LR-HLS IP core.

To run the code in this repository the Vivado must be installed on your machine. 

The link to install Xilinx products:

    https://www.xilinx.com/support/download.html 

Running the command below will allow running the Vivado tools via command line.

    source <Vivado_Install_Dir>/Xilinx/Vivado/2019.x/settings64.sh

Clone helper GitLab repository into your local machine:

    git clone https://gitlab.cern.ch/mghorban1/lr_make_hls_ip.git

Clone L1Trigger GitLab repository into your local machine:

    git clone https://gitlab.cern.ch/mghorban1/L1Trigger.git

The helper lr_make_hls_ip repository contains:

1) lr_make_hls_ip/hls:

- The add_links file, which creates a working directory for HLS and links all the necessary files from CMSSW_x_x_x environment by creating symbolic links to files.

- The vivado_hls_script.tcl to change the FPGA parameters if required.

- Vivado_hls_directives.tcl to link the directives used in HLS. (optional)

- A testbench.cc file to test the LR HLS design. 

- The result.golden.dat file which is used to compare the generated and expected results by creating or overwriting the result.dat during HLS run.

2) lr_make_hls_ip/hdl:

- The make_xci.tcl to specify the IP name and its location.

- The vivado_script.tcl to specify and edit Vivado commands and FPGA board.

- A top.xdc to change the clock frequency, if necessary.

- A top.vhd wrapper for Vivado project in VHDL.

The L1Trigger repository contains:

1) The L1Trigger repository contains added HLS directories and all necessary files to run LR-HLS in both interface and src directories in CMSSW_x_x_x.

Note: So far the execution of the code is compatible with CMSSW_10_4_0.

To run Vivado HLS successfully set the environment variables for CMSSW:

    /cvmfs/cms.cern.ch/cmsset_default.sh

Copy CMSSW_x_x_x in local machine:

    cmsrel CMSSW_x_x_x

Set $CMSSW used in add_links script and creating symbolic links: 

    export $CMSSW=/.../CMSSW_xx_xx_xx/src

Change the directory to CMSSW_x_x_x/src and copy the L1Trigger repository by

    git clone https://gitlab.cern.ch/mghorban1/L1Trigger.git

Note: Two directories lr_make_hls_ip and CMSSW_x_x_x/src/L1Trigger/... will be in sync by symbolic links.


To compile the code and create an IP block, run the command ./add_links in hls directory. Change directory to newly generated directory 'workspace' and run the Vivado command:

    vivado_hls -f vivado_hls_script.tcl

After the command is executed successfully, the reports can be accessed via 'LRHLS/solution_1' subdirectories. 

To view the generated waveform in Vivado Simulator use the Waveform Configuration File (.wcfg) in 'LRHLS/solution_1/sim'. If modelsim is preferred the Wave Log Format (.wlf) file must be generated first by modifying the vivado_script.tcl file by adding modelsim to cosim_design command:

    cosim_design -trace_level all -rtl vhdl -tool modelsim   

Optionally, open Vivado GUI with the command;

    vivado_hls -p LRHLS


To use generated IP core in a Vivado project run command below in hdl directory:

    vivado -mode batch -source make_xci.tcl


--- For more information please email: Maziar.Ghorbani@brunel.ac.uk


 
