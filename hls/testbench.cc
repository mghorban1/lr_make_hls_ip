#include <iostream>
#include "LRHLS_top.h"

int main() {

	SettingsHLS *settings = new SettingsHLS();

	// settings
	settings->lrResidPhi_ = 0.001;
	settings->lrResidZ2S_ = 2.5;
	settings->lrResidZPS_ = 0.07;
	settings->lrMinLayers_ = 4;
	settings->lrMinLayersPS_ = 2;
	settings->lrNumIterations_ = 12;
	settings->trackerNumLayers_ = 7;

    DataHLS *data = new DataHLS();

    // track 0
    TrackHLS track0;
    track0.qOverPt_ = 0.3241;
    track0.phi_ = 0.1227;
    track0.cot_ = -1.7040;
    track0.z_ = 0.0001;
    track0.secPhi_ = 1;
    track0.secEta_ = 1;
    track0.valid_ = true;

    // stub 0
    StubHLS stub00;
    stub00.r_ = 94.6379;
    stub00.phi_ = -1.8563;
    stub00.z_ = -186.1925;
    stub00.layerId_ = 4;
    stub00.psModule_ = false;
    stub00.barrel_ = false;
    stub00.valid_ = true;
    track0.stubs_.push_back(stub00);

    // stub 1
    StubHLS stub01;
    stub01.r_ = 54.4911;
    stub01.phi_ = -1.7860;
    stub01.z_ = -107.5146;
    stub01.layerId_ = 6;
    stub01.psModule_ = true;
    stub01.barrel_ = true;
    stub01.valid_ = true;
    track0.stubs_.push_back(stub01);

    // stub 2
    StubHLS stub02;
    stub02.r_ = 107.5262;
    stub02.phi_ = -1.8870;
    stub02.z_ = -220.7665;
    stub02.layerId_ = 5;
    stub02.psModule_ = false;
    stub02.barrel_ = false;
    stub02.valid_ = true;
    track0.stubs_.push_back(stub02);

    // stub 3
    StubHLS stub03;
    stub03.r_ = 66.8174;
    stub03.phi_ = -1.8055;
    stub03.z_ = -130.3275;
    stub03.layerId_ = 2;
    stub03.psModule_ = false;
    stub03.barrel_ = false;
    stub03.valid_ = true;
    track0.stubs_.push_back(stub03);

    // stub 4
    StubHLS stub04;
    stub04.r_ = 77.4936;
    stub04.phi_ = -1.8291;
    stub04.z_ = -155.8525;
    stub04.layerId_ = 3;
    stub04.psModule_ = false;
    stub04.barrel_ = false;
    stub04.valid_ = true;
    track0.stubs_.push_back(stub04);

    // stub 5
    StubHLS stub05;
    stub05.r_ = 66.8667;
    stub05.phi_ = -1.8071;
    stub05.z_ = -129.0175;
    stub05.layerId_ = 2;
    stub05.psModule_ = false;
    stub05.barrel_ = false;
    stub05.valid_ = true;
    track0.stubs_.push_back(stub05);

    // stub 6
    StubHLS stub06;
    stub06.r_ = 23.8376;
    stub06.phi_ = -1.7306;
    stub06.z_ = -45.4597;
    stub06.layerId_ = 0;
    stub06.psModule_ = true;
    stub06.barrel_ = true;
    stub06.valid_ = true;
    track0.stubs_.push_back(stub06);

    // stub 7
    StubHLS stub07;
    stub07.r_ = 37.3041;
    stub07.phi_ = -1.7552;
    stub07.z_ = -72.6815;
    stub07.layerId_ = 1;
    stub07.psModule_ = true;
    stub07.barrel_ = true;
    stub07.valid_ = true;
    track0.stubs_.push_back(stub07);

    // stub 8
    StubHLS stub08;
    stub08.r_ = 27.5178;
    stub08.phi_ = -1.7380;
    stub08.z_ = -52.9256;
    stub08.layerId_ = 0;
    stub08.psModule_ = true;
    stub08.barrel_ = true;
    stub08.valid_ = true;
    track0.stubs_.push_back(stub08);

    // stub 9
    StubHLS stub09;
    stub09.r_ = 24.4002;
    stub09.phi_ = -1.7317;
    stub09.z_ = -46.3784;
    stub09.layerId_ = 0;
    stub09.psModule_ = true;
    stub09.barrel_ = true;
    stub09.valid_ = true;
    track0.stubs_.push_back(stub09);

    data->tracksMHTHLS_.push_back(track0);

    // track 1
    TrackHLS track1;
    track1.qOverPt_ = 0.1019;
    track1.phi_ = -0.0191;
    track1.cot_ = 2.3077;
    track1.z_ = 0.0001;
    track1.secPhi_ = 1;
    track1.secEta_ = 2;
    track1.valid_ = true;

    // stub 0
    StubHLS stub10;
    stub10.r_ = 59.9087;
    stub10.phi_ = -1.5888;
    stub10.z_ = 129.1350;
    stub10.layerId_ = 2;
    stub10.psModule_ = true;
    stub10.barrel_ = false;
    stub10.valid_ = true;
    track1.stubs_.push_back(stub10);

    // stub 1
    StubHLS stub11;
    stub11.r_ = 102.4645;
    stub11.phi_ = -1.6184;
    stub11.z_ = 220.7665;
    stub11.layerId_ = 5;
    stub11.psModule_ = false;
    stub11.barrel_ = false;
    stub11.valid_ = true;
    track1.stubs_.push_back(stub11);

    // stub 2
    StubHLS stub12;
    stub12.r_ = 54.1627;
    stub12.phi_ = -1.5851;
    stub12.z_ = 116.9843;
    stub12.layerId_ = 6;
    stub12.psModule_ = true;
    stub12.barrel_ = true;
    stub12.valid_ = true;
    track1.stubs_.push_back(stub12);

    // stub 3
    StubHLS stub13;
    stub13.r_ = 83.9253;
    stub13.phi_ = -1.6057;
    stub13.z_ = 183.1775;
    stub13.layerId_ = 4;
    stub13.psModule_ = false;
    stub13.barrel_ = false;
    stub13.valid_ = true;
    track1.stubs_.push_back(stub13);

    // stub 4
    StubHLS stub14;
    stub14.r_ = 24.9742;
    stub14.phi_ = -1.5718;
    stub14.z_ = 54.1725;
    stub14.layerId_ = 0;
    stub14.psModule_ = true;
    stub14.barrel_ = true;
    stub14.valid_ = true;
    track1.stubs_.push_back(stub14);

    // stub 5
    StubHLS stub15;
    stub15.r_ = 71.7898;
    stub15.phi_ = -1.5967;
    stub15.z_ = 154.1475;
    stub15.layerId_ = 3;
    stub15.psModule_ = false;
    stub15.barrel_ = false;
    stub15.valid_ = true;
    track1.stubs_.push_back(stub15);

    // stub 6
    StubHLS stub16;
    stub16.r_ = 50.3493;
    stub16.phi_ = -1.5826;
    stub16.z_ = 108.7668;
    stub16.layerId_ = 6;
    stub16.psModule_ = true;
    stub16.barrel_ = true;
    stub16.valid_ = true;
    track1.stubs_.push_back(stub16);

    // stub 7
    StubHLS stub17;
    stub17.r_ = 38.2017;
    stub17.phi_ = -1.5762;
    stub17.z_ = 82.8066;
    stub17.layerId_ = 1;
    stub17.psModule_ = true;
    stub17.barrel_ = true;
    stub17.valid_ = true;
    track1.stubs_.push_back(stub17);

    // stub 8
    StubHLS stub18;
    stub18.r_ = 61.7547;
    stub18.phi_ = -1.5899;
    stub18.z_ = 133.2250;
    stub18.layerId_ = 2;
    stub18.psModule_ = true;
    stub18.barrel_ = false;
    stub18.valid_ = true;
    track1.stubs_.push_back(stub18);

    // stub 9
    StubHLS stub19;
    stub19.r_ = 84.0335;
    stub19.phi_ = -1.3781;
    stub19.z_ = -73.6084;
    stub19.layerId_ = 0;
    stub19.psModule_ = true;
    stub19.barrel_ = true; 
    stub19.valid_ = true;
    track1.stubs_.push_back(stub19);

    data->tracksMHTHLS_.push_back(track1);

    LRHLS_top(settings, data);

    int i, j;
    int retval = 0;
    FILE *fp;

    fp = fopen("result.dat", "w");

    for(i = 0; i < 2; i++) {
    	for (j = 0; j < 12; j++) {
    		if (data->tracksLRHLS()[i].stubs()[j].valid()) {
				//fprintf (fp, "%d\t", data->tracksLRHLS()[i].stubs()[j].layerId().to_int());
				fprintf (fp, "%d\t", data->tracksLRHLS()[i].stubs()[j].layerId());
			}
    	}
        fprintf (fp, "\n");
    }
    fclose (fp);

    retval = system("diff --brief -w result.dat result.golden.dat");
    if (retval != 0) {
        printf("Test failed !!!\n");
        retval = 1;
    } else {
        printf("Test passed !\n");
    }

    return retval;

}
