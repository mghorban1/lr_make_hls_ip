############################################################
### This file is generated automatically by Vivado HLS.
### Please DO NOT edit it.
### Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
#############################################################
delete_project LRHLS
open_project LRHLS

set_top LRHLS_top

add_files LRHLS_top.h            -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files LRHLS_top.cc           -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files DataHLS.h              -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files DataHLS.cc             -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files SettingsHLS.h          -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files SettingsHLS.cc         -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files LRHLS_v0.h             -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files LRHLS_v0.cc            -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files TrackHLS.h             -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files TrackHLS.cc            -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files StubHLS.h              -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files StubHLS.cc             -cflags "-std=c++11"           -csimflags "-std=c++14"
add_files LRHLS_types.h          -cflags "-std=c++11"           -csimflags "-std=c++14"

add_files -tb result.golden.dat  -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
add_files -tb testbench.cc       -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"

open_solution "solution_1"

set_part {xcvu9p-flgb2104-2-e} -tool vivado
create_clock -period 360MHz -name default
source "./vivado_hls_directives.tcl"
csim_design
csynth_design
cosim_design -trace_level all -rtl vhdl
export_design -rtl vhdl -format ip_catalog -display_name "LRHLS"

exit
