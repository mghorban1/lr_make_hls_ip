--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Thu Feb 13 13:56:19 2020
--Host        : centos-home running 64-bit CentOS Linux release 7.7.1908 (Core)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity top is
  port (
    ap_clk_0 : in STD_LOGIC;
    ap_ctrl_0_done : out STD_LOGIC;
    ap_ctrl_0_idle : out STD_LOGIC;
    ap_ctrl_0_ready : out STD_LOGIC;
    ap_ctrl_0_start : in STD_LOGIC;
    ap_rst_n_0 : in STD_LOGIC;
    dataHLS_tracksLRHLS_cot_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_cot_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_cot_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_phi_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_phi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_phi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_qOverPt_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_qOverPt_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_qOverPt_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_secEta_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_secEta_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_secEta_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_secPhi_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_secPhi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_secPhi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_phi_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_stubs_phi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_phi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_r_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_stubs_r_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_r_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_valid_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_valid_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_valid_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_z_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_stubs_z_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_z_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_valid_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_valid_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_valid_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_z_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_z_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_z_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksMHTHLS_cot_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_cot_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_cot_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_phi_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_phi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_phi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_qOverPt_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_qOverPt_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_qOverPt_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_secEta_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_secEta_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_secEta_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_secPhi_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_secPhi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_secPhi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_r_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_r_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_r_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_z_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_z_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_z_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_valid_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_valid_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_valid_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_z_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_z_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_z_V_0_tvalid : in STD_LOGIC;
    settingsHLS_lrMinLayersPS_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    settingsHLS_lrMinLayers_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    settingsHLS_lrNumIterations_V_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    settingsHLS_lrResidPhi_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_lrResidZ2S_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_lrResidZPS_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_trackerNumLayers_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end top;

architecture BEHAVIORAL of top is

  component LRHLS_IP is
  port (
    ap_clk_0 : in STD_LOGIC;
    ap_rst_n_0 : in STD_LOGIC;
    ap_ctrl_0_start : in STD_LOGIC;
    ap_ctrl_0_done : out STD_LOGIC;
    ap_ctrl_0_idle : out STD_LOGIC;
    ap_ctrl_0_ready : out STD_LOGIC;
    settingsHLS_lrResidPhi_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_lrResidZ2S_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_lrResidZPS_V_0 : in STD_LOGIC_VECTOR ( 45 downto 0 );
    settingsHLS_lrMinLayers_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    settingsHLS_lrMinLayersPS_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    settingsHLS_lrNumIterations_V_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    settingsHLS_trackerNumLayers_V_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    dataHLS_tracksMHTHLS_stubs_r_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_r_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_r_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_phi_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_z_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_z_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_z_V_0_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_layerId_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_psModule_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_barrel_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_stubs_valid_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_qOverPt_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_qOverPt_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_qOverPt_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_phi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_phi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_phi_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_cot_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_cot_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_cot_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_z_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_z_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_z_V_0_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksMHTHLS_secPhi_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_secPhi_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_secPhi_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_secEta_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_secEta_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_secEta_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksMHTHLS_valid_V_0_tvalid : in STD_LOGIC;
    dataHLS_tracksMHTHLS_valid_V_0_tready : out STD_LOGIC;
    dataHLS_tracksMHTHLS_valid_V_0_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_secEta_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_secEta_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_secEta_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_z_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_z_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_z_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_barrel_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_secPhi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_secPhi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_secPhi_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_phi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_phi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_phi_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_stubs_phi_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_phi_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_phi_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_psModule_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_z_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_z_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_z_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_qOverPt_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_qOverPt_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_qOverPt_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_valid_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_valid_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_valid_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_layerId_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dataHLS_tracksLRHLS_stubs_r_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_r_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_r_V_0_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    dataHLS_tracksLRHLS_cot_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_cot_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_cot_V_0_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    dataHLS_tracksLRHLS_stubs_valid_V_0_tvalid : out STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_valid_V_0_tready : in STD_LOGIC;
    dataHLS_tracksLRHLS_stubs_valid_V_0_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component LRHLS_IP;

begin
LRHLS_IP_i: component LRHLS_IP
     port map (
      ap_clk_0 => ap_clk_0,
      ap_ctrl_0_done => ap_ctrl_0_done,
      ap_ctrl_0_idle => ap_ctrl_0_idle,
      ap_ctrl_0_ready => ap_ctrl_0_ready,
      ap_ctrl_0_start => ap_ctrl_0_start,
      ap_rst_n_0 => ap_rst_n_0,
      dataHLS_tracksLRHLS_cot_V_0_tdata(47 downto 0) => dataHLS_tracksLRHLS_cot_V_0_tdata(47 downto 0),
      dataHLS_tracksLRHLS_cot_V_0_tready => dataHLS_tracksLRHLS_cot_V_0_tready,
      dataHLS_tracksLRHLS_cot_V_0_tvalid => dataHLS_tracksLRHLS_cot_V_0_tvalid,
      dataHLS_tracksLRHLS_phi_V_0_tdata(47 downto 0) => dataHLS_tracksLRHLS_phi_V_0_tdata(47 downto 0),
      dataHLS_tracksLRHLS_phi_V_0_tready => dataHLS_tracksLRHLS_phi_V_0_tready,
      dataHLS_tracksLRHLS_phi_V_0_tvalid => dataHLS_tracksLRHLS_phi_V_0_tvalid,
      dataHLS_tracksLRHLS_qOverPt_V_0_tdata(47 downto 0) => dataHLS_tracksLRHLS_qOverPt_V_0_tdata(47 downto 0),
      dataHLS_tracksLRHLS_qOverPt_V_0_tready => dataHLS_tracksLRHLS_qOverPt_V_0_tready,
      dataHLS_tracksLRHLS_qOverPt_V_0_tvalid => dataHLS_tracksLRHLS_qOverPt_V_0_tvalid,
      dataHLS_tracksLRHLS_secEta_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_secEta_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_secEta_V_0_tready => dataHLS_tracksLRHLS_secEta_V_0_tready,
      dataHLS_tracksLRHLS_secEta_V_0_tvalid => dataHLS_tracksLRHLS_secEta_V_0_tvalid,
      dataHLS_tracksLRHLS_secPhi_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_secPhi_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_secPhi_V_0_tready => dataHLS_tracksLRHLS_secPhi_V_0_tready,
      dataHLS_tracksLRHLS_secPhi_V_0_tvalid => dataHLS_tracksLRHLS_secPhi_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_barrel_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_stubs_barrel_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_stubs_barrel_V_0_tready => dataHLS_tracksLRHLS_stubs_barrel_V_0_tready,
      dataHLS_tracksLRHLS_stubs_barrel_V_0_tvalid => dataHLS_tracksLRHLS_stubs_barrel_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_layerId_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_stubs_layerId_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_stubs_layerId_V_0_tready => dataHLS_tracksLRHLS_stubs_layerId_V_0_tready,
      dataHLS_tracksLRHLS_stubs_layerId_V_0_tvalid => dataHLS_tracksLRHLS_stubs_layerId_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_phi_V_0_tdata(23 downto 0) => dataHLS_tracksLRHLS_stubs_phi_V_0_tdata(23 downto 0),
      dataHLS_tracksLRHLS_stubs_phi_V_0_tready => dataHLS_tracksLRHLS_stubs_phi_V_0_tready,
      dataHLS_tracksLRHLS_stubs_phi_V_0_tvalid => dataHLS_tracksLRHLS_stubs_phi_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_psModule_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_stubs_psModule_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_stubs_psModule_V_0_tready => dataHLS_tracksLRHLS_stubs_psModule_V_0_tready,
      dataHLS_tracksLRHLS_stubs_psModule_V_0_tvalid => dataHLS_tracksLRHLS_stubs_psModule_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_r_V_0_tdata(23 downto 0) => dataHLS_tracksLRHLS_stubs_r_V_0_tdata(23 downto 0),
      dataHLS_tracksLRHLS_stubs_r_V_0_tready => dataHLS_tracksLRHLS_stubs_r_V_0_tready,
      dataHLS_tracksLRHLS_stubs_r_V_0_tvalid => dataHLS_tracksLRHLS_stubs_r_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_valid_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_stubs_valid_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_stubs_valid_V_0_tready => dataHLS_tracksLRHLS_stubs_valid_V_0_tready,
      dataHLS_tracksLRHLS_stubs_valid_V_0_tvalid => dataHLS_tracksLRHLS_stubs_valid_V_0_tvalid,
      dataHLS_tracksLRHLS_stubs_z_V_0_tdata(23 downto 0) => dataHLS_tracksLRHLS_stubs_z_V_0_tdata(23 downto 0),
      dataHLS_tracksLRHLS_stubs_z_V_0_tready => dataHLS_tracksLRHLS_stubs_z_V_0_tready,
      dataHLS_tracksLRHLS_stubs_z_V_0_tvalid => dataHLS_tracksLRHLS_stubs_z_V_0_tvalid,
      dataHLS_tracksLRHLS_valid_V_0_tdata(7 downto 0) => dataHLS_tracksLRHLS_valid_V_0_tdata(7 downto 0),
      dataHLS_tracksLRHLS_valid_V_0_tready => dataHLS_tracksLRHLS_valid_V_0_tready,
      dataHLS_tracksLRHLS_valid_V_0_tvalid => dataHLS_tracksLRHLS_valid_V_0_tvalid,
      dataHLS_tracksLRHLS_z_V_0_tdata(47 downto 0) => dataHLS_tracksLRHLS_z_V_0_tdata(47 downto 0),
      dataHLS_tracksLRHLS_z_V_0_tready => dataHLS_tracksLRHLS_z_V_0_tready,
      dataHLS_tracksLRHLS_z_V_0_tvalid => dataHLS_tracksLRHLS_z_V_0_tvalid,
      dataHLS_tracksMHTHLS_cot_V_0_tdata(47 downto 0) => dataHLS_tracksMHTHLS_cot_V_0_tdata(47 downto 0),
      dataHLS_tracksMHTHLS_cot_V_0_tready => dataHLS_tracksMHTHLS_cot_V_0_tready,
      dataHLS_tracksMHTHLS_cot_V_0_tvalid => dataHLS_tracksMHTHLS_cot_V_0_tvalid,
      dataHLS_tracksMHTHLS_phi_V_0_tdata(47 downto 0) => dataHLS_tracksMHTHLS_phi_V_0_tdata(47 downto 0),
      dataHLS_tracksMHTHLS_phi_V_0_tready => dataHLS_tracksMHTHLS_phi_V_0_tready,
      dataHLS_tracksMHTHLS_phi_V_0_tvalid => dataHLS_tracksMHTHLS_phi_V_0_tvalid,
      dataHLS_tracksMHTHLS_qOverPt_V_0_tdata(47 downto 0) => dataHLS_tracksMHTHLS_qOverPt_V_0_tdata(47 downto 0),
      dataHLS_tracksMHTHLS_qOverPt_V_0_tready => dataHLS_tracksMHTHLS_qOverPt_V_0_tready,
      dataHLS_tracksMHTHLS_qOverPt_V_0_tvalid => dataHLS_tracksMHTHLS_qOverPt_V_0_tvalid,
      dataHLS_tracksMHTHLS_secEta_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_secEta_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_secEta_V_0_tready => dataHLS_tracksMHTHLS_secEta_V_0_tready,
      dataHLS_tracksMHTHLS_secEta_V_0_tvalid => dataHLS_tracksMHTHLS_secEta_V_0_tvalid,
      dataHLS_tracksMHTHLS_secPhi_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_secPhi_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_secPhi_V_0_tready => dataHLS_tracksMHTHLS_secPhi_V_0_tready,
      dataHLS_tracksMHTHLS_secPhi_V_0_tvalid => dataHLS_tracksMHTHLS_secPhi_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_barrel_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_stubs_barrel_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_stubs_barrel_V_0_tready => dataHLS_tracksMHTHLS_stubs_barrel_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_barrel_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_barrel_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_layerId_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_stubs_layerId_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_stubs_layerId_V_0_tready => dataHLS_tracksMHTHLS_stubs_layerId_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_layerId_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_layerId_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_phi_V_0_tdata(23 downto 0) => dataHLS_tracksMHTHLS_stubs_phi_V_0_tdata(23 downto 0),
      dataHLS_tracksMHTHLS_stubs_phi_V_0_tready => dataHLS_tracksMHTHLS_stubs_phi_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_phi_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_phi_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_psModule_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_stubs_psModule_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_stubs_psModule_V_0_tready => dataHLS_tracksMHTHLS_stubs_psModule_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_psModule_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_psModule_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_r_V_0_tdata(23 downto 0) => dataHLS_tracksMHTHLS_stubs_r_V_0_tdata(23 downto 0),
      dataHLS_tracksMHTHLS_stubs_r_V_0_tready => dataHLS_tracksMHTHLS_stubs_r_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_r_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_r_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_valid_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_stubs_valid_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_stubs_valid_V_0_tready => dataHLS_tracksMHTHLS_stubs_valid_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_valid_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_valid_V_0_tvalid,
      dataHLS_tracksMHTHLS_stubs_z_V_0_tdata(23 downto 0) => dataHLS_tracksMHTHLS_stubs_z_V_0_tdata(23 downto 0),
      dataHLS_tracksMHTHLS_stubs_z_V_0_tready => dataHLS_tracksMHTHLS_stubs_z_V_0_tready,
      dataHLS_tracksMHTHLS_stubs_z_V_0_tvalid => dataHLS_tracksMHTHLS_stubs_z_V_0_tvalid,
      dataHLS_tracksMHTHLS_valid_V_0_tdata(7 downto 0) => dataHLS_tracksMHTHLS_valid_V_0_tdata(7 downto 0),
      dataHLS_tracksMHTHLS_valid_V_0_tready => dataHLS_tracksMHTHLS_valid_V_0_tready,
      dataHLS_tracksMHTHLS_valid_V_0_tvalid => dataHLS_tracksMHTHLS_valid_V_0_tvalid,
      dataHLS_tracksMHTHLS_z_V_0_tdata(47 downto 0) => dataHLS_tracksMHTHLS_z_V_0_tdata(47 downto 0),
      dataHLS_tracksMHTHLS_z_V_0_tready => dataHLS_tracksMHTHLS_z_V_0_tready,
      dataHLS_tracksMHTHLS_z_V_0_tvalid => dataHLS_tracksMHTHLS_z_V_0_tvalid,
      settingsHLS_lrMinLayersPS_V_0(2 downto 0) => settingsHLS_lrMinLayersPS_V_0(2 downto 0),
      settingsHLS_lrMinLayers_V_0(2 downto 0) => settingsHLS_lrMinLayers_V_0(2 downto 0),
      settingsHLS_lrNumIterations_V_0(3 downto 0) => settingsHLS_lrNumIterations_V_0(3 downto 0),
      settingsHLS_lrResidPhi_V_0(45 downto 0) => settingsHLS_lrResidPhi_V_0(45 downto 0),
      settingsHLS_lrResidZ2S_V_0(45 downto 0) => settingsHLS_lrResidZ2S_V_0(45 downto 0),
      settingsHLS_lrResidZPS_V_0(45 downto 0) => settingsHLS_lrResidZPS_V_0(45 downto 0),
      settingsHLS_trackerNumLayers_V_0(2 downto 0) => settingsHLS_trackerNumLayers_V_0(2 downto 0)
    );
end BEHAVIORAL;
