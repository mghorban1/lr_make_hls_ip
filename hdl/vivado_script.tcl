######################################################################################################
# Used to execute Vivado commands on a project without the Vivado GUI.
#
# To use, type "vivado -mode batch -source vivado_script.tcl"
#
# After editing the code below to specify which Vivado commands you wish to execute.
# (To discover correct TCL commands to use, execute the commands you want within the
#  Vivado GUI and then look at the vivado.jou file).
#
# Also see
#  https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug835-vivado-tcl-commands.pdf
#  https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_4/ug894-vivado-tcl-scripting.pdf
######################################################################################################

#--------- Procedure definitions ----------

#--- Initialize project

proc INIT {firmDir} {
  # Virtex7 FPGA
  #set_property PART xc7vx690tffg1927-3 [current_project]
  
  # Kintex Ultrascale FPGA
  #set_property PART xcku115-flvb1760-2-e [current_project]
  #set_property PART xcku115-flvd1517-2-i [current_project]
  #set_property PART xcku115-flvd1517-3-e [current_project]
  
  # Virtex Ultrascale-Plus FPGA
  set_property PART xcvu9p-flgb2104-2-e [current_project]

  # This optimisation strategy is used in MP7 framework, but has virtually no effect.
  set_property "strategy" "Flow_AreaOptimized_High" [get_runs synth_1]
  set_property "strategy" "Performance_ExplorePostRoutePhysOpt" [get_runs impl_1]
  
  # Good in case timing fails.
  #set_property "strategy" "Flow_AlternateRoutability" [get_runs synth_1]
  #set_property "strategy" "Performance_ExtraTimingOpt" [get_runs impl_1]

  set_property target_language VHDL [current_project]

  if {"[get_files -quiet $firmDir/top.vhd]" == {}} {
    puts "=== Adding .vhd & .xdc files to project"
    add_files -norecurse $firmDir/top.vhd 
    set_property top top [current_fileset]
    add_files -fileset sim_1 -norecurse $firmDir/tb.vhd
    add_files -fileset constrs_1 -norecurse $firmDir/top.xdc
  }

  # Needed if design doesn't connect to I/O pins of FPGA.
  set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

  # Suppress pointless warning messages.
  set_msg_config -quiet -severity INFO -suppress
  #set_msg_config -quiet -id {IP_Flow 19-4830} -suppress
}

#--- Remove user IP

proc RESET_IP {ip_local_dir ip_local_name} {

  set ip_local $ip_local_dir/$ip_local_name/$ip_local_name.xci

  # Check there is actually an IP to reset, since get errors if try resetting non-existant one.
  if { [file exists  $ip_local] == 0 } {
    puts "=== SKIPPING IP RESET: as didn't find $ip_local"
  } else {
    puts "=== RESETTING IP: LOCAL_DIR = $ip_local_dir LOCAL_NAME = $ip_local_name"

    # Remove files from project (in top.xpr), but not from disk.
    remove_files  -fileset $ip_local_name $ip_local
    #remove_files  -fileset sources_1 $ip_local_1
    # Remove files from disk.
    file delete -force $ip_local_dir/${ip_local_name}

    # Delete previously installed user IP repository (not usually necessary)
    set_property  ip_repo_paths  {} [current_project] 
    update_ip_catalog
  } 
}

#--- Add user IP.

proc ADD_IP {ip_repo_dir ip_local_dir ip_repo_name ip_local_name} {
  puts "=== ADDING IP: REPO_DIR = $ip_repo_dir LOCAL_DIR = $ip_local_dir REPO_NAME = $ip_repo_name LOCAL_NAME = $ip_local_name"

  # Add IP repository
  set_property  ip_repo_paths $ip_repo_dir [current_project]
  update_ip_catalog

  set ip_local $ip_local_dir/$ip_local_name/$ip_local_name.xci

    # Create specific IP from repository (makes .xci file)
  create_ip -name $ip_repo_name -vendor xilinx.com -library hls -version 1.0 -module_name $ip_local_name

  generate_target {instantiation_template} [get_files $ip_local]

  generate_target all [get_files  $ip_local]

  create_ip_run [get_files -of_objects [get_fileset  [current_fileset] ] $ip_local]

  # Print all IPs used by project
  # report_ip_status
}

#--- Run synthesis locally with specified number of parallel jobs, or run on remote host.

proc RUN_SYNTHESIS {} {
  puts "=== RUNNING SYNTHESIS"
 
  set runName "synth_1"

# Check if synthesis ran successfully
  set RA [get_property STATUS [get_runs $runName] ]
# Check if it met timing.
  set RB [get_property STATS.WNS [get_runs $runName] ]
  set RC [get_property STATS.WHS [get_runs $runName] ]
  puts "=== PRE-SYNTHESIS STATUS = $RA , WNS = $RB, WHS = $RC"

  launch_runs $runName -jobs 4
  # launch_runs $runName -host {vivado 4}  
  wait_on $runName
  puts "=== SYNTHESIS DONE"

# Get synthesis results
  open_run $runName -name $runName
  report_timing_summary -file timing_$runName.rpt
  report_utilization -file util_$runName.rpt

# Check if synthesis ran successfully
  set RA [get_property STATUS [get_runs $runName] ]

# Check if it met timing.
  set RB [get_property STATS.WNS [get_runs $runName] ]
  set RC [get_property STATS.WHS [get_runs $runName] ]
  puts "=== SYNTHESIS STATUS = $RA , WNS = $RB, WHS = $RC"
}

#--- Run implementation locally with specified number of parallel jobs, or run on remote host.

proc RUN_IMPLEMENTATION {} {
  puts "=== RUNNING IMPLEMENTATION"
 
  set runName "impl_1"
  launch_runs $runName -jobs 4
  
  # launch_runs $runName -host {vivado 4}  
  wait_on $runName
  puts "=== IMPLEMENTATION DONE"

# Get implementation results
  open_run $runName -name $runName
  report_timing_summary -file timing_$runName.rpt
  report_utilization -file util_$runName.rpt

# Check if implementation ran successfully
  set RA [get_property STATUS [get_runs $runName] ]

# Check if it met timing.
  set RB [get_property STATS.WNS [get_runs $runName] ]
  set RC [get_property STATS.WHS [get_runs $runName] ]
  puts "=== IMPLEMENTATION STATUS = $RA , WNS = $RB, WHS = $RC"
}

#--------- TOP-LEVEL TCL SCRIPT ----------


# Vivado project name.
set projName LR_useHLS_IP/LR_useHLS_IP.xpr

#if { [file exists $projName ] == 0} {
#  # Project doesn't already exist
#
#  #puts "=== ERROR: file $projName doesn't exist ==="
#  #exit
# 
#  puts "=== Creating new project $projName ==="
#  create_project -verbose KF_useHLS_IP KF_useHLS_IP
#
#} else {
  # Project already exists.

  #puts "=== Opening  project $projName ==="
  #open_project $projName

  puts "=== Creating/replacing project $projName ==="
  create_project -verbose -force LR_useHLS_IP LR_useHLS_IP
#} 

puts "=== PROJECT OPENED $projName"

puts "The time is: [clock format [clock seconds] -format %H:%M:%S]"

# Get top-level directory of Vivado project
set topDir [get_property DIRECTORY [current_project] ]

# And directory containing firmware.
set firmDir $topDir/../

# And directory containing HLS IP core
set hlsIPDir ../hls/workspace/LRHLS/solution_1/impl/ip

# Initialization (only needs to be done once)
INIT $firmDir

puts "=== RESET PROJECT COMPILATION & OUTPUT"
reset_project -verbose
#reset_simulation sim_1 -mode behavioural 
#reset_simulation sim_1 -mode post-implementation -type functional 
#reset_run synth_1
#reset_run impl_1

# Delete previously installed user IP.
RESET_IP \
  $topDir/LR_useHLS_IP.srcs/[current_fileset]/ip \
  LRHLS_IP

# Add user IP
ADD_IP \
  $hlsIPDir \
  $topDir/LR_useHLS_IP.srcs/[current_fileset]/ip \
  LRHLS_top \
  LRHLS_IP

#  LRHLS \ 3rd line above

update_compile_order -verbose

#puts "=== COMPILE ORDER [get_files -compile_order sources -used_in synthesis]"

# Run synthesis & implentation. 
RUN_SYNTHESIS
RUN_IMPLEMENTATION

# Optionally print out info on all known clocks.
#open_run impl_1
#report_clocks -file report_clocks.txt

puts "The time is: [clock format [clock seconds] -format %H:%M:%S]"

exit
