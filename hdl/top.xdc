# 240 MHz
#create_clock -period 4.166 -name ap_clk -waveform {0.000 2.083} [get_ports ap_clk]

# 320 MHz
create_clock -period 3.124 -name ap_clk -waveform {0.000 1.562} [get_ports ap_clk]

# 360 MHz
#create_clock -period 2.778 -name ap_clk -waveform {0.000 1.389} [get_ports ap_clk]

# 420 MHz
#create_clock -period 2.380 -name ap_clk -waveform {0.000 1.190} [get_ports ap_clk]

# 440 MHz
#create_clock -period 2.272 -name ap_clk -waveform {0.000 1.136} [get_ports ap_clk]

# 480 MHz
#create_clock -period 2.084 -name ap_clk -waveform {0.000 1.042} [get_ports ap_clk]